<?php
  if (is_numeric($_POST['first']) && is_numeric($_POST['second'])) {
    $result = $_POST['first'] + $_POST['second'];
    $class = 'result';
  }
  else {
    $result = "Please enter numbers.";
    $class = 'non-numeric';
  }
?>

<html>
<head>
  <title>My firsty firt php app.</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="nav">
  <?php
    require_once '../toc.php';
  ?>
</div>
<div class="content">
  <h1 class="title">Your sum.</h1>
  <?php print "<div class=$class> $result </div>"; ?>

  <div class="again">
    <p>Add more numbers if you like:</p>
    <?php require_once('add_two_numbers.php'); ?>
  </div>
</div><!--end of div.content -->
</body>
</html>
