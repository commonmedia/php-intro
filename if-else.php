<html>
<head>
  <title>if/else statements</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="nav">
  <?php
    require_once 'toc.php';
  ?>
</div>
<h1>if/else statements</h1>
<div class="content">
  <p>
    An if else statement tests some <code class="inline">condition</code> and
    behaves one way <code class="inline">if</code> the condition is
    <code class="inline">true</code> and has a different behaviour if the
    condition is <code class="inline">false</code>.
  </p>
  <hr />
  <h3>Condition</h3>
  <p>
    <code>
      7 == 12 // false
    </code>
    <br />
    <br />
    <code>
      'elbo' == 'elbo' // true
    </code>
    <br />
    <br />
    Determine the value of this condition:<br />
    <code>
      $x = 15.7;
      $x < 16.8; // true or false?
    </code>
  </p>
  <hr>
  <h3>Your first if/else statement</h3>
  <code>
    <pre>
      $my_var = 10;
      if ($my_var == 10) {
        print 'You are the best';
      }
      else {
        print 'No way.';
      }
    </pre>
  </code>
</div><!-- end of div.content -->
</body>
</html>
