<html>
<head>
  <title>Functions</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="nav">
  <?php
    require_once 'toc.php';
  ?>
</div>
<div class="content">
  <h1 class="title">Functions</h1>
  <p>
    A function is a block of code in PHP and most programming languages that is
    grouped together to accomplish some task.
  </p>
  <p>
    For example you might have a function called <code class="inline">roll_die()</code>
    that returns the value of a roll of a di.
  </p>
  <p>
    Example die roll: <span class="die-roll"><?php print roll_die(); ?></span>
  </p>
  <?php
  /**
   * Return an integer that represents the roll of one die.
   */
    function roll_die() {
      return rand(1, 6);
    }
  ?>
</div><!-- end of div.content -->
</body>
</html>
