<html>
<head>
  <title>My firsty firt php app.</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="nav">
  <?php
    require_once 'toc.php';
  ?>
</div>
<div class="content">
  <h1 class="title">Prerequisites</h1>
  <ul>
    <li>A computer with at least Apache and PHP installed</li>
    <li>A text editor or an IDE</li>
    <ul>
      <li><a href="http://www.vim.org/">vim</a></li>
      <li><a href="https://atom.io/">Atom</a></li>
      <li><a href="https://www.jetbrains.com/phpstorm/">PhpStorm</a></li>
    </ul>
  </ul>
</div>
</body>
</html>
