<html>
<head>
  <title>My firsty firt php app.</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="nav">
  <?php
    require_once 'toc.php';
  ?>
</div>
<div class="content">
  <h1 class="title">Basic Syntax</h1>
  <p>
    Every PHP block starts with an opening <code>&lt;?php</code> tag and ends with a closing
    <code>?&gt;</code> tag.
 </p>
 <p>
   For example to output the text hello world! to the screen.  You would open a PHP block,
   use the <code>print</code> command, type your <code>string</code>, and close your PHP block.
   <br />
   <div class="code">
   <code class="block">
     &lt;?php <br /><br />
       &nbsp;&nbsp; print "Hello World!"; <br />
     ?&gt;
   </code>
  </div>
 </p>
</div>
</body>
</html>
