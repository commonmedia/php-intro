<html>
<head>
  <title>More PHP Resources</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="nav">
  <?php
    require_once 'toc.php';
  ?>
</div>
<div class="content">
  <h1 class="title">More PHP Resources</h1>
  <p>
    This repo {php, html, css}:
    <br />
    <a href="https://bitbucket.org/commonmedia/php-intro/">https://bitbucket.org/commonmedia/php-intro/</a>
  </p>
  <p>
    PHP resources on the internet:
    <br />
    <a href="http://php.net">php.net</a>
    <br />
    <br />
    <strong>CMS</strong>
    <br />
    <a href="https://backdropcms.org">https://backdropcms.org</a>
    <br />
    &nbsp;&nbsp;<a href="https://api.backdropcms.org">https://api.backdropcms.org</a>
    <br />
    <br />
    <a href="https://drupal.org">https://drupal.org</a>
    <br />
    &nbsp;&nbsp;<a href="https://api.drupal.org">https://api.drupal.org</a>
    <br />
    <br />
    <strong>Frameworks</strong>
    <br />
    <a href="http://laravel.com/">http://laravel.com/</a>
    <br />
    &nbsp;&nbsp;<a href="https://laracasts.com/series/laravel-5-fundamentals">Laracasts</a>
    <br />
    <br />
    <a href="https://symfony.com/">https://symfony.com/</a>
    <br />
  </p>
  
</div>
</body>
</html>
