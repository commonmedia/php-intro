<html>
<head>
  <title>My firsty firt PHP app.</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="nav">
  <?php
    require_once 'toc.php';
  ?>
</div>
<div class="content">
  <h1 class="title">My firsty first PHP app.</h1>
  <p>
    This is the first PHP application we will build.  It:
    <ol>
      <li>Takes two inputs from the user (hopefully numbers).</li>
      <li>Adds them together</li>
      <li>Displays the result</li>
    </ol>

  </p>
  <?php
   /*$form = <<<HTML
    <form action="my_form.php" method="post">
      Add two numbers.
      <input type="text" name="first" size="3" /> +
      <input type="text" name="second" size="3" />
      <br />
      <input type="submit" value="Add" />
    </form>
HTML;

  print $form;*/
  require_once 'bin/add_two_numbers.php';
  ?>
</div>
</body>
</html>
