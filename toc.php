<nav>
Learn the fundamentals of PHP programming.

<ul>
  <li><a href="/prerequisites.php">Prerequisites</a></li>
  <li><a href="/index.php">A first PHP Application</a></li>
  <li>
    <a href="/basic_syntax.php">Basic Syntax</a>
    <ul>
      <li><a href="variables.php">Variables</a></li>
      <li><a href="if-else.php">if/else</a></li>
      <li>for/foreach</li>
      <li>while/do while</li>
      <li><a href="functions.php">functions</a></li>
    </ul>
  </li>
  <li>
    HTML Forms
    <ul>
      <li>HTML Form Elements</li>
      <li>action: processing forms</li>
    </ul>
  </li>
    <li>Classes/Objects</li>
    <li>PHP Frameworks</li>
    <li>Git Versions Control</li>.
    <li><a href="/more.php"">More resources / PHP as a Career</a></li>
  </ul>
</nav>
