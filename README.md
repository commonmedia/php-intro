Introduction to PHP
---

* Prerequisites
  * A first PHP Application
  
* Basic Syntax
  * Variables
  * if/else
  * for/foreach
  * while/do while
  * functions
  
* HTML Forms
  * HTML Form Elements
  * action: processing forms
  
* More resources / PHP as a Career