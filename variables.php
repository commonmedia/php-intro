<html>
<head>
  <title>Variables</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="nav">
  <?php
    require_once 'toc.php';
  ?>
</div>
<div class="content">
  <h1 class="title">Variables</h1>
  <p>
    In php variables start with a <code>$</code> sign.  For example: <code>$my_var</code>.
    <br /><br />
    Variables have types which refers to the type of data that it holds. Some types:
    <ul>
      <li class="variables">String: <code>$line = "Some text it may or may not have numbers in it like 1 or 2.";</code></li>
      <li class="variables">
        Numbers:
          <ul>
            <li class="variables">int: <code>$x = 3;</code> :: <code>$x</code> stores the integer 3.</li>
            <li class="variables">float: <code>$num = 2.73;</code> :: floats can store decimal values.</li>
          </ul>
      </li>
      <li class="variables">Array: <code>$list = array(1, 2, 7);</code> :: <code>$list</code> contains a list of values.</li>
    </ul>
  </p>
  <hr>
  <h2>Dynamically Typed</h2>
  <p>
    This just means that we don't have to tell PHP ahead of time what we are going to use a
    variable to store.  PHP just figures it out for us.
    <br /><br />
    There are quite a few other types in PHP, you can read about them here: <a href="http://php.net/manual/en/language.types.php">http://php.net/manual/en/language.types.php</a>, but
    for our purposes we just need to know that PHP can use these types.
  </p>
</div><!-- end of div.content -->
</body>
</html>
